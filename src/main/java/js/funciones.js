function inicio(){
    document.getElementById("mainSlider").style.display = 'block';
    document.getElementById("ribbon").style.display = 'block';
    document.getElementById("juegos").style.display = 'none';
    document.getElementById("juegos movil").style.display = 'none';
    document.getElementById("tcg").style.display = 'none';
}

function juegos(){
    document.getElementById("mainSlider").style.display = 'none';
    document.getElementById("ribbon").style.display = 'none';
    document.getElementById("juegos").style.display = 'block';
    document.getElementById("juegos movil").style.display = 'none';
    document.getElementById("tcg").style.display = 'none';
}

function juegosMovil(){
    document.getElementById("mainSlider").style.display = 'none';
    document.getElementById("ribbon").style.display = 'none';
    document.getElementById("juegos").style.display = 'none';
    document.getElementById("juegos movil").style.display = 'block';
    document.getElementById("tcg").style.display = 'none';
}

function tcg(){
    document.getElementById("mainSlider").style.display = 'none';
    document.getElementById("ribbon").style.display = 'none';
    document.getElementById("juegos").style.display = 'none';
    document.getElementById("juegos movil").style.display = 'none';
    document.getElementById("tcg").style.display = 'block';
}